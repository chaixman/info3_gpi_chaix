package warmup.tree.tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import warmup.tree.Leaf;
import warmup.tree.Node;
import warmup.tree.NotFoundException;
import warmup.tree.Tree;

/*
 * Have a look at these online resources:
 * 
 *  https://junit.org/junit4/
 *    
 * A small JUnit Cook Book:
 *    http://junit.sourceforge.net/doc/cookbook/cookbook.htm
 * 
 * The Eclipse Guide "JUnit Get Started":
 *    http://help.eclipse.org/mars/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2FgettingStarted%2Fqs-junit.htm   
 * 
 */

public class OurTest {
  
  @Before
  public void before() {
    // Executed before each test
  }

  @After
  public void after() {
    // Executed after each test
  }

  // Use this if you want a global timeout of 10 seconds.
  // @Rule
  // public Timeout globalTimeout = Timeout.seconds(10); // 10 seconds max per method tested

  // Use this if you need a timeout,
  //    @Test(timeout=1000)
  // but be careful when debugging, the timeout will trigger
  // while debugging...
  @Test
  public void test1() throws NotFoundException {
    Tree tree = new Tree();
    String path = "/toto/titi/tata".replace('/', Tree.pathSeparator);
    Node n1 = tree.makePath(path,true);
    String path2 = "/toto/titi/titi".replace('/', Tree.pathSeparator);
    Node n2 = tree.makePath(path2,true);
    assertTrue(n1.parent() == n2.parent());
  }
  
  @Test
  public void test2() throws NotFoundException {
    Tree tree = new Tree();
    Node n1 = new Node(tree,"toto");
    assertTrue(n1.parent()==tree);
    Node n2 = new Node(n1,"titi");
    assertTrue(n2.parent()==n1);
    Node n3= new Leaf(n2,"tata"); 
    tree.makePath("/toto/titi/tata", true);
  }
  
  @Test(expected=NotFoundException.class)
  public void test3() throws NotFoundException {
    Tree tree = new Tree();
    String path = "/toto/titi/tata".replace('/', Tree.pathSeparator);
    Node n1 = tree.makePath(path,true);
    n1.remove();
    tree.find("/toto/titi/tata");
  }
  
  @Test
  public void test4() throws NotFoundException {
    Tree tree = new Tree();
    String path;
    Node n1, n2;
    path = "/toto/titi/tutu".replace('/', Tree.pathSeparator);
    n1 = tree.makePath(path, true);
    n2 = tree.makePath(path,true);
    assertTrue(n1 == n2);
  }
  
  @Test
  public void test5() throws NotFoundException {
	    Tree tree = new Tree();
	    Node n1 = new Node(tree,"a");
	    assertTrue(n1.parent()==tree);
	    Node n2 = new Node(n1,"b");
	    assertTrue(n2.parent()==n1);
	    Node n3 = new Node(n2,"c");
	    assertTrue(n3.parent()==n2);
	    // path exists, but as a node
	    // expected to throw IllegalStateException
	    String path;
	    path = "/a/b/c/b".replace('/', Tree.pathSeparator);
	    assertTrue(tree.makePath(path,true) instanceof Leaf);  
	  }
  
  public void test6() throws NotFoundException {
	    Tree tree = new Tree();
	    String path = "/toto/titi/tata".replace('/', Tree.pathSeparator);
	    Node n1 = tree.makePath(path, false);
	    assertTrue("".equals(tree.name()));
	    assertTrue(tree.find(path) == n1);
	  }
}