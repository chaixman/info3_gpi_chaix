package warmup.tree;

/**
 * This is a tree of named node.
 * 
 * @author Pr. Olivier Gruber (olivier dot gruber at acm dot org)
 */
public class Tree extends Node {

	public static final char pathSeparator = '/';
	public static final String pathSeparatorString = "/";

	public Tree() {
		super("");
	}

	/**
	 * Finds a node corresponding to a path in the tree. If the path does not
	 * exists, throws NotFoundException
	 * 
	 * @param path of the searched node
	 * @return the node named by the given path
	 * @throws NotFoundException if the path does not exist
	 */
	public Node find(String path) throws NotFoundException {
		String[] chemin = path.split(pathSeparatorString);
		if(path.charAt(0) == pathSeparator) {
			Node n = this;
			for (int i = 1; i < chemin.length; i++) {
				n = n.child(chemin[i]);
				if (n == null) {
					throw new NotFoundException(path);
				}
			}
			return n;
		}
		throw new IllegalArgumentException();
	}

	/**
	 * Make a path in the tree, leading either to a leaf or to a node.
	 * 
	 * @throws IllegalStateException if the path should be to a leaf but it already
	 *                               exists to a node, of if the path should be to a
	 *                               node but it already exists to a leaf.
	 */
	public Node makePath(String path, boolean isLeaf) {
		String[] chemin = path.split(pathSeparatorString);
		int i = 1;
		Node n = this;
		int lg = chemin.length;

		while (i < lg && !(n.child(chemin[i]) == null)) {
			n = n.child(chemin[i]);
			i++;
		}
		if (i != lg && !(n instanceof Leaf)) {
			while (i < lg - 1) {
				Node nouveau = new Node(n, chemin[i]);
				n = nouveau;
				i++;
			}
			Node fin;
			if (isLeaf) {
				fin = new Leaf(n, chemin[i]);
			} else {
				fin = new Node(n, chemin[i]);
			}
			return fin;
		} else {
			if ((i != lg && n instanceof Leaf) || (isLeaf && !(n instanceof Leaf)) || (!isLeaf && n instanceof Leaf)) {
				throw new IllegalStateException();
			} else {
				return n;
			}
		}
	}

	public String toString() {
		TreePrinter p = new TreePrinter(this);
		return p.toString();
	}

}
