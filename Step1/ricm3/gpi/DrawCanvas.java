package ricm3.gpi;

import ricm3.gpi.gui.layout.Container;
import ricm3.gpi.gui.layout.Location;
import ricm3.gpi.gui.widgets.Canvas;

import java.util.LinkedList;

import ricm3.gpi.gui.Color;
import ricm3.gpi.gui.Graphics;
import ricm3.gpi.gui.KeyListener;
import ricm3.gpi.gui.MouseListener;

/**
 * A canvas to draw lines freely.
 * 
 * To draw a line, two possibilities.
 * 
 * 1) Press down the left button and move the mouse The drawing ends when the
 * left button is released.
 * 
 * 2) Press the space-bar key on the keyboard toggles the drawing mode. Moving
 * the mouse draws if drawing is on.
 * 
 * To clear the canvas, press the key 'c' on your keyboard.
 * 
 * @author Pr. Olivier Gruber (olivier dot gruber at acm dot org)
 */
public class DrawCanvas extends Canvas {

	protected Location depart;
	protected Location arrive;
	protected LinkedList<Location> ligne;
	protected boolean drawing;

	public DrawCanvas(Container parent) {
		super(parent);
		setMouseListener(new ClickListener());
		setKeyListener(new ClickListener());
		ligne = new LinkedList<Location>();
	}

	@Override
	public void paint(Graphics g) {
		g.setColor(m_bgColor);
		Location loc = new Location(0, 0);
		g.fillRect(loc.x(), loc.y(), m_width, m_height);
		int taille = ligne.size();
		if (taille % 2 == 0) {
			g.setColor(Color.BLACK);
			for (int i = 0; i < taille; i += 2) {
				g.drawLine(ligne.get(i).x(), ligne.get(i).y(), ligne.get(i + 1).x(), ligne.get(i + 1).y());
			}
		}
	}

	class ClickListener implements MouseListener, KeyListener {

		ClickListener() {};

		@Override
		public void mousePressed(int x, int y, int buttons) {
			if (buttons == 1) {
				depart = new Location(x, y);
				toLocal(depart);
				ligne.add(depart);

			}
		}

		@Override
		public void mouseReleased(int x, int y, int buttons) {
			if (buttons == 1) {
				arrive = new Location(x, y);
				toLocal(arrive);
				ligne.add(arrive);
				repaint();
			}
		}

		@Override
		public void mouseEntered(int x, int y) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited() {

		}

		@Override
		public void mouseMoved(int x, int y) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyPressed(char k, int code) {
			if (code == KeyListener.VK_C) {
				ligne.clear();
				repaint();
			}
			
		}

		@Override
		public void keyReleased(char k, int code) {
			// TODO Auto-generated method stub
			
		}
	}
}
