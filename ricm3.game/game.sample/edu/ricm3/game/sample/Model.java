/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.imageio.ImageIO;

import edu.ricm3.game.GameModel;

public class Model extends GameModel {
	BufferedImage m_cowboySprite, m_bg1, m_bg2, m_sol, m_ghostSprite, m_coeur;
	LinkedList<Ghost> m_ghosts;
	Cowboy m_cowboys;
	Random rand = new Random();
	Overhead m_overhead = new Overhead();
	int action = 0;
	int haut_p = 600;
	int nsaut;

	public Model() {
		loadSprites();
		m_cowboys = new Cowboy(this, 0, m_cowboySprite, 4, 6, 100, haut_p - 122, 3F);
		m_ghosts = new LinkedList<Ghost>();
		for (int i = 0; i < Options.NGHOST; i++) {
			m_ghosts.add(new Ghost(this, i, m_ghostSprite, 4, 4, rand.nextInt(700), rand.nextInt(200), 2F));	
		}
		nsaut = 0;
	}

	@Override
	public void shutdown() {

	}

	public Overhead getOverhead() {
		return m_overhead;
	}

	public Cowboy cowboys() {
		return m_cowboys;
	}

	public Iterator<Ghost> ghosts() {
		return m_ghosts.iterator();
	}

	/**
	 * Simulation step.
	 * 
	 * @param now is the current time in milliseconds.
	 */
	public void step(long now) {
		Iterator<Ghost> iterG = m_ghosts.iterator();
		while (iterG.hasNext()) {
			Ghost g = iterG.next();
			g.step(now);
		}
		m_overhead.overhead();
		switch (action) {
		case 0x25:
			m_cowboys.goLeft(now);
			action = 0;
			break;
		case 0x27:
			m_cowboys.goRight(now);
			action = 0;
			break;
		case 0x26:
			if (m_cowboys.auSol(now)) {
				action = 1000;
			}
			break;
		case 1000:
			if (nsaut < 30) {
				m_cowboys.Jump(now, nsaut);
				nsaut++;
			} else {
				nsaut = 0;
				action = 0;
			}
			break;
		default:
			m_cowboys.step(now);
		}
	}

	private void loadSprites() {
		/*
		 * Cowboy with rifle, western style; png; 48x48 px sprite size Krasi Wasilev (
		 * http://freegameassets.blogspot.com)
		 */
		File imageFile = new File("game.sample/sprites/winchester.png");
		try {
			m_cowboySprite = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

		imageFile = new File("game.sample/sprites/4.png");
		try {
			m_bg1 = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

		imageFile = new File("game.sample/sprites/5.png");
		try {
			m_bg2 = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

		imageFile = new File("game.sample/sprites/sol3.png");
		try {
			m_sol = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

		imageFile = new File("game.sample/sprites/ghost.png");
		try {
			m_ghostSprite = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

		imageFile = new File("game.sample/sprites/coeur.png");
		try {
			m_coeur = ImageIO.read(imageFile);
		} catch (IOException ex) {
			ex.printStackTrace();
			System.exit(-1);
		}

	}

}
