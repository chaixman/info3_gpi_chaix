package edu.ricm3.game.sample;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Ghost {

	// ATTRIBUTS
	BufferedImage m_sprite;
	int startMat;
	int incr = 0;
	int m_w, m_h;
	int m_x, m_y;
	int m_nrows, m_ncols;
	int m_step;
	int m_nsteps;
	int m_idx;
	float m_scale;
	long m_lastMove, m_lastReverse;
	int moov;
	BufferedImage[] m_sprites;
	Model m_model;
	int widthWindow;
	int heightWindow;

	// CONSTRUCTEUR
	Ghost(Model model, int no, BufferedImage sprite, int rows, int columns, int x, int y, float scale) {
		m_model = model;
		m_sprite = sprite;
		m_ncols = columns;
		m_nrows = rows;
		m_x = x;
		m_y = y;
		m_scale = scale;
		splitSprite();
	}

	// MÉTHODES

	void splitSprite() {
		int width = m_sprite.getWidth(null);
		int height = m_sprite.getHeight(null);
		m_sprites = new BufferedImage[m_nrows * m_ncols];
		m_w = width / m_ncols;
		m_h = height / m_nrows;
		m_step = 8;
		for (int i = 0; i < m_nrows; i++) {
			for (int j = 0; j < m_ncols; j++) {
				int x = j * m_w;
				int y = i * m_h;
				m_sprites[(i * m_ncols) + j] = m_sprite.getSubimage(x, y, m_w, m_h);
			}
		}
	}

	void step(long now) {
		Random rand = new Random();
		moov = rand.nextInt(8);
		long elapsed = now - m_lastMove;
		if (elapsed > 50L) {
			m_lastMove = now;
			if ((m_x >= 0 && m_x <= widthWindow - (m_w * m_scale))
					&& (m_y >= 0 && m_y <= heightWindow - (m_h * m_scale))) {
				switch (moov) {
				case 0:
					m_x += 10;
					m_idx = rand.nextInt(4) + 8;
					break;

				case 1:
					m_x -= 10;
					m_idx = rand.nextInt(4) + 4;
					break;

				case 2:
					m_y -= 10;
					m_idx = rand.nextInt(4);
					break;

				case 3:
					m_y += 10;
					m_idx = rand.nextInt(4);
					break;

				case 4:
					m_x += 10;
					m_y -= 10;
					m_idx = rand.nextInt(4) + 8;
					break;

				case 5:
					m_x -= 10;
					m_y += 10;
					m_idx = rand.nextInt(4) + 4;
					break;

				case 6:
					m_x += 10;
					m_y += 10;
					m_idx = rand.nextInt(4) + 8;
					break;

				case 7:
					m_x -= 10;
					m_y -= 10;
					m_idx = rand.nextInt(4) + 4;
					break;
				}
			} else if (m_x < 0 || m_y < 0 || (m_x < 0 && m_y < 0)) {
				m_x += 20;
				m_y += 20;
			} else if (m_x > widthWindow - (m_w * m_scale) || m_y > heightWindow - (m_h * m_scale)
					|| (m_x > widthWindow - (m_w * m_scale) && m_y > heightWindow - (m_h * m_scale))) {
				m_x -= 20;
				m_y -= 20;
			} else if (m_x < 0 && m_y > heightWindow - (m_h * m_scale)) {
				m_x += 30;
				m_y -= 30;
			} else if (m_x > widthWindow - (m_w * m_scale) && m_y < 0) {
				m_x -= 30;
				m_y += 30;
			}

		}
	}

	void paint(Graphics g) {
		Image img = m_sprites[m_idx];
		int w = (int) (m_scale * m_w);
		int h = (int) (m_scale * m_h);
		g.drawImage(img, m_x, m_y, w, h, null);

	}
}
