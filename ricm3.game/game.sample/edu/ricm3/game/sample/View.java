/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Iterator;

import edu.ricm3.game.GameView;

public class View extends GameView {

	private static final long serialVersionUID = 1L;

	Color m_background = Color.white;
	long m_last;
	int m_npaints;
	int m_fps;
	Model m_model;
	int last_y;
	// Controller m_ctr;

	public View(Model m) {
		m_model = m;
		// m_ctr = c;
	}

	public void step(long now) {

	}

	private void computeFPS() {
		long now = System.currentTimeMillis();
		if (now - m_last > 1000L) {
			m_fps = m_npaints;
			m_last = now;
			m_npaints = 0;
		}
		m_game.setFPS(m_fps, null);
		// m_game.setFPS(m_fps, "npaints=" + m_npaints);
		m_npaints++;
	}

	@Override
	protected void _paint(Graphics g) {
		computeFPS();

		// background
		g.drawImage(m_model.m_bg1, 0, 0, getWidth(), getHeight(), null);
		g.drawImage(m_model.m_bg2, 0, 0, getWidth(), getHeight(), null);

		// FLOOR
		Platform floor = new Platform(0, m_model.haut_p, getWidth(), getHeight() - m_model.haut_p);
		floor.img = m_model.m_sol;
		floor._paint(g);

		// PLATFORM 1
		Platform p1 = new Platform(getWidth() / 2 - 150, 2 * getHeight() / 6, 300, 30);
		p1.img = m_model.m_sol;
		p1._paint(g);

		// PLATFORM 2
		Platform p2 = new Platform(getWidth() / 2 - 300, 4 * getHeight() / 6 - 40, 200, 30);
		p2.img = m_model.m_sol;
		p2._paint(g);

		// PLATFORM 3
		Platform p3 = new Platform(1 * getWidth() / 2 + 110, 4 * getHeight() / 6 - 40, 200, 30);
		p3.img = m_model.m_sol;
		p3._paint(g);

		Cowboy cowboys = m_model.m_cowboys;
		cowboys.f_w = getWidth();
		cowboys.f_h = getHeight();
		
		cowboys.paint(g);

		Iterator<Ghost> iter2 = m_model.ghosts();
	    while (iter2.hasNext()) {
	      Ghost f = iter2.next();
	      f.heightWindow = getHeight();
	      f.widthWindow = getWidth();
	      f.paint(g);
	    }
	}
}
